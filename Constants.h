#pragma once

#include <array>

struct LayerInfo {
	float z;
	unsigned int nColsPerSide;
	unsigned int nRowsPerSide;
	float invHalfSectorYSize;
	float invHalfSectorXSize;
	float dxDy;
};

extern float zMidUT;
extern float distToMomentum;
extern std::array<float, 124> fudgeFactors;
extern std::array<float, 3751> bdlTable;
extern std::array<LayerInfo, 4> layersInfos;
extern std::array<std::array<std::array<std::array<float, 98>, 3>, 2>, 2> sectorsZ;
