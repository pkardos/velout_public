#pragma once

#include "DataStructures.h"
#include "Hlt1Track.h"

#include <tuple>
#include <vector>


namespace opt {

class UtHitSpacePartition;



// Output data of the velo ut algorithm.
using PrVeloUtOutput = std::tuple<UtHits,
								  std::vector<uint32_t, simd_alloc<uint32_t>>,
								  States,
								  std::vector<LHCb::HLT1::Track>>;


// Velo ut algorithm.
class PrVeloUtOpt {
public:
	PrVeloUtOutput operator()(const std::vector<LHCb::HLT1::Track>& veloTracks,
							  const States& veloEndStates,
							  const UtHitSpacePartition& hits) const;

private:
	static ExtrapolatedStates ExtrapolateStates(const States& states, float z);

	static auto FilterStates(const std::vector<LHCb::HLT1::Track>& veloTracks,
							 const States& veloEndStates) -> std::tuple<std::vector<LHCb::HLT1::Track>, States>;

	static StateBinLocations GetBinLocations(const UtHitSpacePartition& hits, const ExtrapolatedStates& layerStates, size_t& totalNumBins);
	static std::pair<GatheredHits, std::vector<uint32_t>> GatherHits(const UtHitSpacePartition& hits, const States& states, const ExtrapolatedStates& layerStates, float tolerance);
	static std::pair<GatheredHits, std::vector<uint32_t>> GatherHits(const UtHitSpacePartition& hits, const States& states, const ExtrapolatedStates* layerStates, size_t numLayerStates, float tolerance);

public:
	mutable size_t binSizeSum = 0;
	mutable size_t binCountSum = 0;

	static inline float gatherWindow = 1.4f;
	static inline float matchWindow = 1.4f;
	static inline float matchOffsetWindow = 0.5f;

	static constexpr float matchingYFront = 2330.f;
	static constexpr float matchingYBack = 2660.f;
};



} // namespace opt