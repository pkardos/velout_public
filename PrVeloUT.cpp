/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Include files
//#include "UTDet/DeUTSector.h"

// local
#include "PrVeloUT.h"

#include "Hlt1Track.h"

#include <fstream>
#include <sstream>

//-----------------------------------------------------------------------------
// Implementation file for class : PrVeloUT
//
// 2007-05-08 : Mariusz Witek
// 2017-03-01: Christoph Hasse (adapt to future framework)
//-----------------------------------------------------------------------------



void findSectors(unsigned int layer, float x, float y, float xTol, float yTol,
				 const LayerInfo& info,
				 std::vector<std::pair<int, int>>& sectors) {

	constexpr static const auto mapQuarterSectorToSectorCentralRegion = std::array{
		6, 6, 9, 9, 10, 10, 13, 13,
		7, 7, 8, 8, 11, 11, 12, 12,
		25, 25, 26, 28, 31, 33, 34, 34,
		24, 24, 27, 29, 30, 32, 35, 35,
		46, 46, 49, 51, 52, 54, 57, 57,
		47, 47, 48, 50, 53, 55, 56, 56,
		69, 69, 70, 70, 73, 73, 74, 74,
		68, 68, 71, 71, 72, 72, 75, 75
	};

	constexpr static const auto mapSectorToSector = std::array{
		1, 2, 3, 4, 5, 0, 0, 0, 0, 14, 15, 16, 17, 18,
		19, 20, 21, 22, 23, 0, 0, 0, 0, 36, 37, 38, 39, 40,
		41, 42, 43, 44, 45, 0, 0, 0, 0, 58, 59, 60, 61, 62,
		63, 64, 65, 66, 67, 0, 0, 0, 0, 76, 77, 78, 79, 80
	};


	auto localX = x - info.dxDy * y;
	// deal with sector overlaps and geometry imprecision
	xTol += 1; //mm
	auto localXmin = localX - xTol;
	auto localXmax = localX + xTol;
	int subcolmin = std::round(localXmin * info.invHalfSectorXSize - 0.5) + 2 * info.nColsPerSide;
	int subcolmax = std::round(localXmax * info.invHalfSectorXSize - 0.5) + 2 * info.nColsPerSide;
	if (subcolmax < 0 || subcolmin >= (int)(4 * info.nColsPerSide)) {
		// out of acceptance, return empty result
		return;
	}
	// on the acceptance limit
	if (subcolmax >= (int)(4 * info.nColsPerSide))
		subcolmax = (int)(4 * info.nColsPerSide) - 1;
	if (subcolmin < 0)
		subcolmin = 0;
	// deal with sector shifts in tilted layers and overlaps in regular ones
	yTol += (layer == 1 || layer == 2) ? 8 : 1; //  mm
	auto localYmin = y - yTol;
	auto localYmax = y + yTol;
	int subrowmin = std::round(localYmin * info.invHalfSectorYSize - 0.5) + 2 * info.nRowsPerSide;
	int subrowmax = std::round(localYmax * info.invHalfSectorYSize - 0.5) + 2 * info.nRowsPerSide;
	if (subrowmax < 0 || subrowmin >= (int)(4 * info.nRowsPerSide)) {
		// out of acceptance, return empty result
		return;
	}
	// on the acceptance limit
	if (subrowmax >= (int)(4 * info.nRowsPerSide))
		subrowmax = (int)(4 * info.nRowsPerSide) - 1;
	if (subrowmin < 0)
		subrowmin = 0;
	for (int subcol = subcolmin; subcol <= subcolmax; subcol++) {
		int region = subcol < (int)(2 * info.nColsPerSide - 4) ? 1 : subcol >= (int)(2 * info.nColsPerSide + 4) ? 3 : 2;
		if (region == 1) {
			for (int subrow = subrowmin; subrow <= subrowmax; subrow++) {
				sectors.emplace_back(1, (subcol / 2) * info.nRowsPerSide * 2 + subrow / 2 + 1);
			}
		}
		else if (region == 2) {
			int subcolInReg = subcol - 2 * info.nColsPerSide + 4;
			for (int subrow = subrowmin; subrow <= subrowmax; subrow++) {
				if (subrow < (int)(2 * info.nRowsPerSide - 4) || subrow >= (int)(2 * info.nRowsPerSide + 4)) {
					// no in central Region
					sectors.emplace_back(2, mapSectorToSector[(subcolInReg / 2) * 14 + (subrow / 2)]);
				}
				else {
					// central region
					sectors.emplace_back(2, mapQuarterSectorToSectorCentralRegion[subcolInReg * 8 + subrow - 2 * info.nRowsPerSide + 4]);
				}
			}
		}
		else {
			for (int subrow = subrowmin; subrow <= subrowmax; subrow++) {
				sectors.emplace_back(3, (subcol / 2 - info.nColsPerSide - 2) * info.nRowsPerSide * 2 + subrow / 2 + 1);
			}
		}
	}
}



namespace {

//perform a fit using trackhelper's best hits with y correction, improve qop estimate
float fastfitter(const TrackHelper& helper, float zKink, float zMidUT) {
	const float zDiff = 0.001 * (zKink - zMidUT);
	float mat[6] = { helper.wb, helper.wb * zDiff, helper.wb * zDiff * zDiff, 0.0, 0.0, helper.wb };
	float rhs[3] = { helper.wb * helper.xMidField, helper.wb * helper.xMidField * zDiff, 0.0 };

	for (auto hit : helper.bestHits) {
		if (hit == nullptr) {
			continue;
		}
		const float ui = hit->x;
		const float dz = 0.001 * (hit->z - zMidUT);
		const float w = hit->HitPtr->weight();
		const float t = SinUtFiberAngle(*(hit->HitPtr));

		mat[0] += w;
		mat[1] += w * dz;
		mat[2] += w * dz * dz;
		mat[3] += w * t;
		mat[4] += w * dz * t;
		mat[5] += w * t * t;
		rhs[0] += w * ui;
		rhs[1] += w * ui * dz;
		rhs[2] += w * ui * t;
	}

	// TODO:: use LUP solver from Mathter
	//ROOT::Math::CholeskyDecomp<float, 3> decomp(mat);
	//if (!decomp)
	//{
	//  return helper.bestParams[0];
	//}
	//else
	//{
	//  decomp.Solve(rhs);
	//}
	mathter::Matrix<float, 3, 3> A = {
		mat[0], mat[1], mat[3],
		mat[1], mat[2], mat[4],
		mat[3], mat[4], mat[5]
	};
	mathter::Vector<float, 3> b = { rhs[0], rhs[1], rhs[2] };
	mathter::Vector<float, 3> x = A.DecompositionLUP().Solve(b);
	rhs[0] = b[0];
	rhs[1] = b[1];
	rhs[2] = b[2];

	const float xSlopeUTFit = 0.001 * rhs[1];
	const float xUTFit = rhs[0];

	// new VELO slope x
	const float xb = xUTFit + xSlopeUTFit * (zKink - zMidUT);
	const float xSlopeVeloFit = (xb - helper.state.x) * helper.invKinkVeloDist;

	// calculate q/p
	const float sinInX = xSlopeVeloFit * rsqrt(1. + xSlopeVeloFit * xSlopeVeloFit);
	const float sinOutX = xSlopeUTFit * rsqrt(1. + xSlopeUTFit * xSlopeUTFit);
	return (sinInX - sinOutX);
}

bool isVeloTrackBackwards(const LHCb::HLT1::Track& veloTrack) {
	float z1 = veloTrack.veloSegment->front().z;
	float z2 = veloTrack.veloSegment->back().z;
	return z2 < z1;
}

// -- These things are all hardcopied from the PrTableForFunction
// -- and PrUTMagnetTool
// -- If the granularity or whatever changes, this will give wrong results

int masterIndex(const int index1, const int index2, const int index3) {
	return (index3 * 11 + index2) * 31 + index1;
}

template <typename>
constexpr bool false_v = false;


constexpr auto minValsBdl = std::array{ -0.3f, -250.0f, 0.0f };
constexpr auto maxValsBdl = std::array{ 0.3f, 250.0f, 800.0f };
constexpr auto deltaBdl = std::array{ 0.02f, 50.0f, 80.0f };
constexpr auto dxDyHelper = std::array{ 0.0f, 1.0f, -1.0f, 0.0f };
} // namespace

/// Initialization
void PrVeloUT::initialize() {
	// m_zMidUT is a position of normalization plane which should to be close to z middle of UT ( +- 5 cm ).
	// Cached once in PrVeloUTTool at initialization. No need to update with small UT movement.
	m_zMidUT = 0.0f;
	assert(false);
	// zMidField and distToMomentum isproperly recalculated in PrUTMagnetTool when B field changes
	m_distToMomentum = 0.0f;
	assert(false);
}

void PrVeloUT::recomputeGeometry() {
	// LHCb::UTDAQ::computeGeometry(*m_utDet, m_layers, m_sectorsZ);
	assert(false);
}


template <int N>
float FindKink(const TrackHelper& helper) {
	using Vec2 = mathter::Vector<float, 2>;
	using Line2 = mathter::Line<float, 2>;

	mathter::Matrix<float, N, 2> A;
	mathter::Vector<float, N> b;
	mathter::Vector<float, 2> x;

	for (int i = 0; i < N; ++i) {
		A(i, 0) = 1;
		A(i, 1) = helper.bestHits[i]->z;
		b(i) = helper.bestHits[i]->x;
	}

	x = (A.Transposed() * A).DecompositionLUP().Solve(A.Transposed() * b);

	float errtotal = 0.0f;
	for (int i = 0; i < N; ++i) {
		float err = helper.bestHits[i]->x - (x[1] * helper.bestHits[i]->z + x[0]);
		errtotal += err * err;
	}


	Line2 lineOfTrack(Vec2(helper.state.z, helper.state.x), Vec2(1.0f, helper.state.tx).Normalized());
	Line2 lineOfUt(Vec2(0.0f, x[0]), Vec2(1.0f, x[1]).Normalized());


	auto intersection = Intersect(lineOfTrack, lineOfUt);
	assert(intersection.Intersecting());
	//std::cout << "tx = " << helper.state.tx
	//		  << "   \tkink = " << intersection.Point().x
	//		  << "   \tbend = " << mathter::Rad2Deg(acos(abs(Dot(lineOfTrack.Direction(), lineOfUt.Direction()))))
	//		  << "   \terr = " << sqrt(errtotal)
	//		  << std::endl;

	return intersection.Point().x;
}

//=============================================================================
// Main execution
//=============================================================================
PrVeloUtOutput PrVeloUT::operator()(const std::vector<LHCb::HLT1::Track>& veloTracks,
									const std::vector<LHCb::State>& veloEndStates,
									const UT::HitHandler& hh) const {
	// All VELO tracks that were extended through the UT.
	PrVeloUtOutputWorking output;

	const auto& fudgeFactors = ::fudgeFactors;
	const auto& bdlTable = ::bdlTable;

	std::array<UT::Mut::Hits, 4> hitsInLayers;
	for (auto& it : hitsInLayers) {
		it.reserve(8); // TODO: check if 8 is correct.
	}

	for (size_t i = 0; i < veloTracks.size(); ++i) {
		const LHCb::HLT1::Track& veloTrack = veloTracks[i];
		const LHCb::State& veloState = veloEndStates[i];

		MiniState veloEndState{ (float)veloState.x(), (float)veloState.y(), (float)veloState.z(), (float)veloState.tx(), (float)veloState.ty() };

		// Skip unusable tracks.
		if (isVeloTrackBackwards(veloTrack) || isTowardsBeampipe(veloEndState) || isMissingUt(veloEndState)) {
			continue;
		}

		for (auto& it : hitsInLayers) {
			it.clear();
		}
		if (!getHits(hitsInLayers, hh, fudgeFactors, veloEndState))
			continue;

		TrackHelper helper(veloEndState, m_zKink, m_sigmaVeloSlope, m_maxPseudoChi2);

		if (!formClusters(hitsInLayers, helper)) {
			std::reverse(hitsInLayers.begin(), hitsInLayers.end());
			formClusters(hitsInLayers, helper);
			std::reverse(hitsInLayers.begin(), hitsInLayers.end());
		}

		if (helper.bestHits[0]) {
			auto outTrack = makeSlimTrack(helper, hitsInLayers, bdlTable);
			if (outTrack) {
				//std::cout << i << "\n";
				auto& [hits, state] = outTrack.value();
				auto modVeloState = veloState;
				modVeloState.setQOverP(state.qOverP());
				output.hits.push_back(std::move(hits));
				output.endVeloStates.push_back(veloState);
				output.atUtStates.push_back(state);
				output.tracks.push_back(veloTrack);

				//std::cout << (helper.bestHits[3] ? 4 : 3);
				float kink = helper.bestHits[3] ? FindKink<4>(helper) : FindKink<3>(helper);
				//std::cout << ", " << kink << std::endl;
			}
		}
	}

	for (size_t i = 0; i < output.tracks.size(); ++i) {
		output.tracks[i].utSegment = &output.hits[i];
	}

	// Update counters.
	m_seedsCounter += veloTracks.size();
	m_tracksCounter += output.hits.size();

	//std::cout << "inline const std::vector<LHCb::State> g_outUtStates = {" << std::endl;
	//for (const LHCb::State& state : output.atUtStates) {
	//	std::cout << "LHCb::State{"
	//	        << "{" << state.x() << ", "
	//	        << state.y() << ", "
	//	        << state.tx() << ", "
	//	        << state.ty() << ", "
	//	        << state.qOverP() << "}, "
	//	        << state.z() << ""
	//	        <<"},\n";
	//}
	//std::cout << "};\n\n";


	return { std::move(output.hits), std::move(output.endVeloStates), std::move(output.atUtStates), std::move(output.tracks) };
}


//=============================================================================
// Find the hits
//=============================================================================
template <typename FudgeTable>
bool PrVeloUT::getHits(std::array<UT::Mut::Hits, 4>& hitsInLayers,
					   const UT::HitHandler& hh,
					   const FudgeTable& fudgeFactors, const MiniState& trState) const {

	// -- This is hardcoded, so faster
	// -- If you ever change the Table in the magnet tool, this will be wrong
	const float absSlopeY = std::abs(trState.ty);
	const int index = (int)(absSlopeY * 100 + 0.5);
	float(&normFact)[4] = (float(&)[4])fudgeFactors[4 * index];

	// -- this 500 seems a little odd...
	const float invTheta = std::min(500., 1.0 / std::sqrt(trState.tx * trState.tx + trState.ty * trState.ty));
	const float minMom = std::max(m_minPT * invTheta, m_minMomentum);
	const float xTol = std::abs(1. / (m_distToMomentum * minMom));
	const float yTol = m_yTol + m_yTolSlope * xTol;

	int nLayers = 0;
	std::vector<std::pair<int, int>> sectors;
	sectors.reserve(9);

	for (int iStation = 0; iStation < 2; ++iStation) {

		if (iStation == 1 && nLayers == 0) {
			return false;
		}

		for (int iLayer = 0; iLayer < 2; ++iLayer) {
			if (iStation == 1 && iLayer == 1 && nLayers < 2)
				return false;

			const unsigned int layerIndex = 2 * iStation + iLayer;
			const float z = m_layers[layerIndex].z;
			const float yAtZ = trState.y + trState.ty * (z - trState.z);
			const float xLayer = trState.x + trState.tx * (z - trState.z);
			const float yLayer = yAtZ + yTol * m_layers[layerIndex].dxDy;
			const float normFactNum = normFact[layerIndex];
			const float invNormFact = 1.0 / normFactNum;

			findSectors(layerIndex, xLayer, yLayer,
						xTol * invNormFact - std::abs(trState.tx) * m_intraLayerDist,
						m_yTol + m_yTolSlope * std::abs(xTol * invNormFact),
						m_layers[layerIndex], sectors);

			const auto& sectorsZForLayer = m_sectorsZ[iStation][iLayer];
			std::pair pp{ -1, -1 };
			for (auto& p : sectors) {
				// sectors can be duplicated in the list, but they are ordered
				if (p == pp)
					continue;
				pp = p;
				findHits(hh.hits(iStation + 1, iLayer + 1, p.first, p.second),
						 sectorsZForLayer[p.first - 1][p.second - 1], trState,
						 xTol * invNormFact, invNormFact, hitsInLayers[layerIndex]);
			}
			sectors.clear();
			nLayers += int(!hitsInLayers[2 * iStation + iLayer].empty());
		}
	}

	return nLayers > 2;
}

//=========================================================================
// Form clusters
//=========================================================================
bool PrVeloUT::formClusters(const std::array<UT::Mut::Hits, 4>& hitsInLayers, TrackHelper& helper) const {

	bool fourLayerSolution = false;

	for (const auto& hit0 : hitsInLayers[0]) {

		const float xhitLayer0 = hit0.x;
		const float zhitLayer0 = hit0.z;

		// Loop over Second Layer
		for (const auto& hit2 : hitsInLayers[2]) {

			const float xhitLayer2 = hit2.x;
			const float zhitLayer2 = hit2.z;

			const float tx = (xhitLayer2 - xhitLayer0) / (zhitLayer2 - zhitLayer0);

			if (std::abs(tx - helper.state.tx) > m_deltaTx2)
				continue;

			const UT::Mut::Hit* bestHit1 = nullptr;
			float hitTol = m_hitTol2;
			for (auto& hit1 : hitsInLayers[1]) {

				const float xhitLayer1 = hit1.x;
				const float zhitLayer1 = hit1.z;

				const float xextrapLayer1 = xhitLayer0 + tx * (zhitLayer1 - zhitLayer0);
				if (std::abs(xhitLayer1 - xextrapLayer1) < hitTol) {
					hitTol = std::abs(xhitLayer1 - xextrapLayer1);
					bestHit1 = &hit1;
				}
			}

			if (fourLayerSolution && !bestHit1)
				continue;

			const UT::Mut::Hit* bestHit3 = nullptr;
			hitTol = m_hitTol2;
			for (auto& hit3 : hitsInLayers[3]) {

				const float xhitLayer3 = hit3.x;
				const float zhitLayer3 = hit3.z;

				const float xextrapLayer3 = xhitLayer2 + tx * (zhitLayer3 - zhitLayer2);

				if (std::abs(xhitLayer3 - xextrapLayer3) < hitTol) {
					hitTol = std::abs(xhitLayer3 - xextrapLayer3);
					bestHit3 = &hit3;
				}
			}

			// -- All hits found
			if (bestHit1 && bestHit3) {
				simpleFit(std::array{ &hit0, bestHit1, &hit2, bestHit3 }, helper);

				if (!fourLayerSolution && helper.bestHits[0]) {
					fourLayerSolution = true;
				}
				continue;
			}

			// -- Nothing found in layer 3
			if (!fourLayerSolution && bestHit1) {
				simpleFit(std::array{ &hit0, bestHit1, &hit2 }, helper);
				continue;
			}
			// -- Noting found in layer 1
			if (!fourLayerSolution && bestHit3) {
				simpleFit(std::array{ &hit0, bestHit3, &hit2 }, helper);
				continue;
			}
		}
	}

	return fourLayerSolution;
}


//=========================================================================
// Create the Velo-UT tracks
//=========================================================================

template <class BdlTable>
auto PrVeloUT::makeSlimTrack(const TrackHelper& helper,
							 const std::array<UT::Mut::Hits, 4>& hitsInLayers,
							 const BdlTable& bdlTable) const
	-> std::optional<std::tuple<std::vector<UT::Hit>, LHCb::State>> {
	//== Handle states. copy Velo one, add TT.
	const float zOrigin = (std::fabs(helper.state.ty) > 0.001) ? helper.state.z - helper.state.y / helper.state.ty : helper.state.z - helper.state.x / helper.state.tx;

	//const float bdl1    = m_PrUTMagnetTool->bdlIntegral(helper.state.ty,zOrigin,helper.state.z);

	// -- These are calculations, copied and simplified from PrTableForFunction
	// -- FIXME: these rely on the internal details of PrTableForFunction!!!
	//           and should at least be put back in there, and used from here
	//           to make sure everything _stays_ consistent...
	const auto var = std::array{ helper.state.ty, zOrigin, helper.state.z };

	const int index1 = std::max(0, std::min(30, int((var[0] + 0.3) / 0.6 * 30)));
	const int index2 = std::max(0, std::min(10, int((var[1] + 250) / 500 * 10)));
	const int index3 = std::max(0, std::min(10, int(var[2] / 800 * 10)));

	float bdl = bdlTable[masterIndex(index1, index2, index3)];

	const auto bdls = std::array{ bdlTable[masterIndex(index1 + 1, index2, index3)],
								  bdlTable[masterIndex(index1, index2 + 1, index3)],
								  bdlTable[masterIndex(index1, index2, index3 + 1)] };

	const auto boundaries = std::array{ -0.3f + float(index1) * deltaBdl[0],
										-250.0f + float(index2) * deltaBdl[1],
										0.0f + float(index3) * deltaBdl[2] };

	// -- This is an interpolation, to get a bit more precision
	float addBdlVal = 0.0;
	for (int i = 0; i < 3; ++i) {

		if (var[i] < minValsBdl[i] || var[i] > maxValsBdl[i])
			continue;

		const float dTab_dVar = (bdls[i] - bdl) / deltaBdl[i];
		const float dVar = (var[i] - boundaries[i]);
		addBdlVal += dTab_dVar * dVar;
	}
	bdl += addBdlVal;
	// ----

	const float qpxz2p = -1 * rsqrt(1. + helper.state.ty * helper.state.ty) / bdl * 3.3356 / Gaudi::Units::GeV;
	const float qp = m_finalFit ? fastfitter(helper, m_zKink, m_zMidUT) : helper.bestParams[0];
	const float qop = (std::abs(bdl) < 1.e-8) ? 0.0 : qp * qpxz2p;

	// -- Don't make tracks that have grossly too low momentum
	// -- Beware of the momentum resolution!
	const float p = 1.3 * std::abs(1 / qop);
	const float pt = p * std::sqrt(helper.state.tx * helper.state.tx + helper.state.ty * helper.state.ty);

	if (p < m_minMomentum || pt < m_minPT)
		return {};

	const float txUT = helper.bestParams[3];
	const float xUT = helper.bestParams[2];

	std::vector<UT::Hit> hits;
	LHCb::State stateAtUt;

	// Adding overlap hits
	for (const auto* hit : helper.bestHits) {
		// -- only the last one can be a nullptr.
		if (!hit)
			break;

		hits.push_back(*hit->HitPtr);

		const float xhit = hit->x;
		const float zhit = hit->z;

		for (auto& ohit : hitsInLayers[hit->HitPtr->planeCode()]) {
			const float zohit = ohit.z;
			if (zohit == zhit)
				continue;

			const float xohit = ohit.x;
			const float xextrap = xhit + txUT * (zohit - zhit);
			if (xohit - xextrap < -m_overlapTol)
				continue;
			if (xohit - xextrap > m_overlapTol)
				break;

			hits.push_back(*ohit.HitPtr);
			// -- only one overlap hit
			//break;
		}
	}

	const float yMidUT = helper.state.y + helper.state.ty * (m_zMidUT - helper.state.z);

	//== Add a new state...
	stateAtUt.setX(xUT);
	stateAtUt.setY(yMidUT);
	stateAtUt.setZ(m_zMidUT);
	stateAtUt.setTx(txUT);
	stateAtUt.setTy(helper.state.ty);
	stateAtUt.setQOverP(qop);
	//  stateAtUt.setState( xUT,
	//                yMidUT,
	//                m_zMidUT,
	//                txUT,
	//                helper.state.ty,
	//                qop);

	return std::tuple<std::vector<UT::Hit, std::allocator<UT::Hit>>, LHCb::State>{ std::move(hits), stateAtUt };
}



bool PrVeloUT::isTowardsBeampipe(const MiniState& state) const {
	// Particle coordinates at the middle of the UT.
	const float xMidUT = state.x + state.tx * (m_zMidUT - state.z);
	const float yMidUT = state.y + state.ty * (m_zMidUT - state.z);

	// Skip if middle UT coordinates fall inside central hole.
	return xMidUT * xMidUT + yMidUT * yMidUT < m_centralHoleSize * m_centralHoleSize;
}

bool PrVeloUT::isMissingUt(const MiniState& state) const {
	return std::abs(state.tx) > m_maxXSlope || std::abs(state.ty) > m_maxYSlope;
}
