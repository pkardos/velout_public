#pragma once

#include "State.h"
#include "UTMutHit.h"

#include <AlignedAllocator.hpp>
#include <Soa/SoaContainerify.hpp>

template <class T>
using simd_alloc = AlignedAllocator<T, 16, false>;

// Soa structure to store states.
struct StatesBase {
	std::vector<float, simd_alloc<float>> x, y, tx, ty, z, qop;
};
using States = SoaContainerify<StatesBase>;


// Soa structure to store UT hits.
struct UtHitsBase {
	std::vector<float, simd_alloc<float>> x0, y0, z0, y1, dxdy, error;
	std::vector<uint32_t, simd_alloc<uint32_t>> id;
};
using UtHits = SoaContainerify<UtHitsBase>;


// States extrapolated for finding bins that belong to a certain track.
struct ExtrapolatedStatesBase {
	std::vector<float, simd_alloc<float>> x;
	std::vector<float, simd_alloc<float>> y;
};
struct ExtrapolatedStates : SoaContainerify<ExtrapolatedStatesBase> {
	float z;
};


// Location and number of hits in the space partition container for each track.
struct StateBinLocationsBase {
	std::vector<uint32_t, simd_alloc<uint32_t>> index;
	std::vector<uint16_t, simd_alloc<uint16_t>> count;
};
using StateBinLocations = SoaContainerify<StateBinLocationsBase>;


// Simplified hits for finding patterns on the XY plane.
struct GatheredHitsBase {
	std::vector<float, simd_alloc<float>> x;
	std::vector<float, simd_alloc<float>> z;
	std::vector<uint32_t, simd_alloc<uint32_t>> index;
};
using GatheredHits = SoaContainerify<GatheredHitsBase>;


// Data used to fit UT track candidates.
struct UtTrackCandidatesBase {
	std::array<std::vector<float, simd_alloc<float>>, 4> hitAccurateX;
	std::array<std::vector<float, simd_alloc<float>>, 4> hitZ;
	std::array<std::vector<float, simd_alloc<float>>, 4> hitWeight;
	std::array<std::vector<float, simd_alloc<float>>, 4> hitIndex;
	std::vector<float, simd_alloc<float>> trackX;
	std::vector<float, simd_alloc<float>> trackY;
	std::vector<float, simd_alloc<float>> trackZ;
	std::vector<float, simd_alloc<float>> trackTy;
	std::vector<float, simd_alloc<float>> trackTx;
	std::vector<uint32_t, simd_alloc<uint32_t>> trackIndex;
};
using UtTrackCandidates = SoaContainerify<UtTrackCandidatesBase>;


// Fitted track properties. -- useful comment...
struct FittedTrackPropertiesBase {
	std::vector<float, simd_alloc<float>> tx;
	std::vector<float, simd_alloc<float>> chi2;
};
using FittedTrackProperties = SoaContainerify<FittedTrackPropertiesBase>;


States MakeSoaStates(const std::vector<LHCb::State>& states);
UtHits MakeSoaUtHits(const std::vector<UT::Hit>& hits);
std::vector<LHCb::State> MakeNormalStates(const States& states);
std::vector<UT::Hit> MakeNormalUtHits(const UtHits& hits);