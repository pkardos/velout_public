/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STl
#include <vector>
#include <cstdint>
#include <cassert>
#include <cmath>
#include <array>
#include <stdexcept>

// Include files
#include "UTHit.h"
#include "UTChannelID.h"


/**
 *  UTHitHandler contains the hits in the UT detector and the accessor to them
 */
namespace UT
{
  class HitHandler
  {

  private:

    /// Type for hit storage
    using HitVector = std::vector< UT::Hit >;

    /// Internal indices storage for ranges
    using HitIndices    = std::pair< std::size_t, std::size_t >;
    using HitsInRegion  = std::array<HitIndices,98>;
    using HitsInLayer   = std::array<HitsInRegion,3>;
    using HitsInStation = std::array<HitsInLayer,2>;
    using HitsInUT      = std::array<HitsInStation,2>;

  public:
    
    using HitRange = std::pair<HitVector::const_iterator, HitVector::const_iterator>;

  public:
    void AddHit(const UT::Hit& hit) {
      auto station = hit.id.station();
      auto layer = hit.id.layer();
      auto region = hit.id.detRegion();
      auto sector = hit.id.sector();


      auto& indices = m_indices[station - 1][layer - 1][region - 1][sector - 1];

      // if first for this range, set the begin and end indices
      if ( &indices != last_indices )
      {
        // check to see if thi range has been filled previously.
        // If it has, assumed ordering is broken
        assert( indices.first == indices.second );
        // reset indices to current end of container
        indices = { m_allhits.size(), m_allhits.size() };
        // update used last index cache
        last_indices = &indices;
      }

      m_allhits.push_back(hit);
      ++(indices.second);
    }

    /// Reserve size in the overall hit container
    void reserve( const std::size_t nHits )
    {
      m_allhits.reserve( nHits );
    }

    /// Access the range for a given set of hits
    const HitRange hits( unsigned int station, 
                         unsigned int layer, 
                         unsigned int region, 
                         unsigned int sector ) const 
    {
      const auto & indices = m_indices[station - 1][layer - 1][region - 1][sector - 1];
      return HitRange( m_allhits.begin() + indices.first,
                       m_allhits.begin() + indices.second );
    }

    /// get the total number of hits
    HitVector::size_type nbHits() const noexcept 
    {
      return m_allhits.size(); 
    }

  private:

    // Indices for each range
    HitsInUT m_indices;

    // single vector of all hits
    HitVector m_allhits;

    // cache pointer to last indices used
    HitIndices * last_indices = nullptr;

  };
}
