#include "PrVeloUtOpt.h"

#include "Constants.h"
#include "UtHitSpacePartition.h"

#include <chrono>
#include <future>
#include <numeric>

#define SIMDPP_ARCH_X86_AVX
#include <simdpp/simd.h>

namespace opt {


UtTrackCandidates FindLines(const std::array<GatheredHits, 4>& gatheredHits, const std::array<std::vector<uint32_t>, 4>& gatheredOffests, const States& states);
void FitTrackCandidates(const UtTrackCandidates& candidates);
FittedTrackProperties FitTrackCandidatesSimple(const UtTrackCandidates& candidates);

static void CopyUtHit(const UtHits& source, size_t sourceIdx, UtHits& target, size_t targetIdx) {
	target.x0[targetIdx] = source.x0[sourceIdx];
	target.z0[targetIdx] = source.z0[sourceIdx];
	target.dxdy[targetIdx] = source.dxdy[sourceIdx];
	target.y0[targetIdx] = source.y0[sourceIdx];
	target.y1[targetIdx] = source.y1[sourceIdx];
	target.error[targetIdx] = source.error[sourceIdx];
	target.id[targetIdx] = source.id[sourceIdx];
}


static void CopyState(const States& source, size_t sourceIdx, States& target, size_t targetIdx) {
	target.x[targetIdx] = source.x[sourceIdx];
	target.y[targetIdx] = source.y[sourceIdx];
	target.tx[targetIdx] = source.tx[sourceIdx];
	target.ty[targetIdx] = source.ty[sourceIdx];
	target.z[targetIdx] = source.z[sourceIdx];
	target.qop[targetIdx] = source.qop[sourceIdx];
}


static float ExtrapolateLinear(float currentValue, float currentParameter, float dvdp, float targetParameter) {
	return currentValue + (targetParameter - currentParameter) * dvdp;
}


template <class... T>
void CompressedStore(const uint8_t* mask, size_t count, std::pair<const T*, T*>... arrays) {
	constexpr size_t vectorLength = 8;
	constexpr size_t overflowMask = 0xFF;
	size_t readIndex = 0;
	size_t writeIndex = 0;
	size_t maskIndex = 0;
	uint8_t selectionMask = 1;

	for (; readIndex < count; ++readIndex) {
		uint8_t condition = mask[maskIndex] & selectionMask;
		if (condition) {
			std::forward_as_tuple(arrays.second[writeIndex]...) = std::forward_as_tuple(arrays.first[readIndex]...);
			++writeIndex;
		}
		maskIndex += selectionMask >> 7;
		selectionMask = (selectionMask << 1) | (selectionMask >> 7); // rotate right
	}
}


StateBinLocations PrVeloUtOpt::GetBinLocations(const UtHitSpacePartition& hits, const ExtrapolatedStates& layerStates, size_t& totalNumBins) {
	size_t numLayerStates = layerStates.size();
	StateBinLocations binLocations;
	binLocations.resize(numLayerStates);

	totalNumBins = 0;
	for (size_t i = 0; i < numLayerStates; ++i) {
		float x = layerStates.x[i];
		float y = layerStates.y[i];
		auto [index, count] = hits.GetDoubleBin(x, y, layerStates.z);

		binLocations.index[i] = index;
		binLocations.count[i] = count;
		totalNumBins += count;
	}

	return binLocations;
}


PrVeloUtOutput PrVeloUtOpt::operator()(const std::vector<LHCb::HLT1::Track>& veloTracks,
									   const States& veloEndStates,
									   const UtHitSpacePartition& hits) const {
	auto [filteredTracks, filteredStates] = FilterStates(veloTracks, veloEndStates);

	std::array<ExtrapolatedStates, 4> layerStates = {
		ExtrapolateStates(filteredStates, layersInfos[0].z),
		ExtrapolateStates(filteredStates, layersInfos[1].z),
		ExtrapolateStates(filteredStates, layersInfos[2].z),
		ExtrapolateStates(filteredStates, layersInfos[3].z),
	};

	std::array<GatheredHits, 4> gatheredHits;
	std::array<std::vector<uint32_t>, 4> gatheredOffests;

	for (size_t i = 0; i < 4; ++i) {
		auto [h, o] = GatherHits(hits, filteredStates, layerStates[i], 1.4f);
		gatheredHits[i] = std::move(h);
		gatheredOffests[i] = std::move(o);
	}

	auto trackCandidates = FindLines(gatheredHits, gatheredOffests, filteredStates);
	auto fitResults = FitTrackCandidatesSimple(trackCandidates);

	// TODO: do a fit of tracks candidates
	// TODO: calc momentum estimates

	return {};
}

//int trackIdx = 125;
//std::cout << "layer0 = [...\n";
//for (int hitIdx = gatheredOffsets[trackIdx - 1]; hitIdx < gatheredOffsets[trackIdx]; ++hitIdx) {
//	std::cout << "    " << gatheredHits.x[hitIdx] << ", " << gatheredHits.z[hitIdx] << ";...\n";
//}
//std::cout << "];\n\n";

//std::cout << "layer1 = [];" << std::endl;
//std::cout << "layer2 = [];" << std::endl;
//std::cout << "layer3 = [];\n"
//		  << std::endl;

//std::cout << "track = [...\n";
//for (int i = 0; i < 4; ++i) {
//	std::cout << "    " << layerStates[i].x[trackIdx] << ", " << layerStates[i].z << ";...\n";
//}
//std::cout << "];\n\n";
//}


ExtrapolatedStates PrVeloUtOpt::ExtrapolateStates(const States& states, float z) {
	size_t size = states.size();
	ExtrapolatedStates result;
	result.resize(size);
	result.z = z;

	for (size_t i = 0; i < size; ++i) {
		float zDiff = z - states.z[i];
		float x = states.x[i] + zDiff * states.tx[i];
		float y = states.y[i] + zDiff * states.ty[i];
		result.x[i] = x;
		result.y[i] = y;
	}

	return result;
}


auto PrVeloUtOpt::FilterStates(const std::vector<LHCb::HLT1::Track>& veloTracks,
							   const States& veloEndStates)
	-> std::tuple<std::vector<LHCb::HLT1::Track>, States> {

	std::vector<LHCb::HLT1::Track> filteredTracks;
	States filteredStates;
	filteredStates.reserve(size_t(veloEndStates.size() * 0.7f));

	size_t size = veloEndStates.size();
	float z1 = layersInfos[0].z;
	float z2 = layersInfos[3].z;

	for (size_t i = 0; i < size; ++i) {
		float x = veloEndStates.x[i];
		float y = veloEndStates.y[i];
		float z = veloEndStates.z[i];
		float tx = veloEndStates.tx[i];
		float ty = veloEndStates.ty[i];

		float x1 = x + tx * (z1 - z);
		float x2 = x + tx * (z2 - z);
		float y1 = y + ty * (z1 - z);
		float y2 = y + ty * (z2 - z);

		if (detectorLeft < x1 && x1 < detectorRight && detectorBottom < y1 && y1 < detectorTop
			&& detectorLeft < x2 && x2 < detectorRight && detectorBottom < y2 && y2 < detectorTop
			&& veloTracks[i].veloSegment->front().z < veloTracks[i].veloSegment->back().z) {
			filteredTracks.push_back(veloTracks[i]);

			filteredStates.x.push_back(veloEndStates.x[i]);
			filteredStates.y.push_back(veloEndStates.y[i]);
			filteredStates.tx.push_back(veloEndStates.tx[i]);
			filteredStates.ty.push_back(veloEndStates.ty[i]);
			filteredStates.z.push_back(veloEndStates.z[i]);
			filteredStates.qop.push_back(veloEndStates.qop[i]);
		}
	}

	return { std::move(filteredTracks), std::move(filteredStates) };
}


std::pair<GatheredHits, std::vector<uint32_t>> PrVeloUtOpt::GatherHits(const UtHitSpacePartition& hits, const States& states, const ExtrapolatedStates& layerStates, float tolerance) {
	const UtHits& hitsContainer = hits.GetPartitionedHits();
	const size_t numLayerStates = layerStates.size();

	size_t writeIndex = 0;

	size_t sumBinSizes;
	StateBinLocations binLocations = GetBinLocations(hits, layerStates, sumBinSizes);

	GatheredHits gatheredHits;
	std::vector<uint32_t> offsets;
	gatheredHits.resize(sumBinSizes);
	offsets.resize(states.size());

	for (size_t i = 0; i < numLayerStates; ++i) {
		const auto index = binLocations.index[i];
		auto count = binLocations.count[i];
		float trackX = layerStates.x[i];
		float trackY = layerStates.y[i];
		float trackTx = states.tx[i];
		float trackTy = states.ty[i];
		float trackZ = layerStates.z;

		constexpr float adaptiveToleranceConstant = 83.f; // This is a magic number obtained from old UT. Roughly 1/500*distToMomentum*minPt, I don't know what it means or if it's needed.
		float adaptiveTolerance = adaptiveToleranceConstant * std::sqrt(trackTx * trackTx + trackTy * trackTy);
		adaptiveTolerance = std::clamp(adaptiveTolerance, 0.2f, 16.f); // Limits from old UT as well.

		while (count) {
			auto totalIndex = index + count;

			const float hitZ0 = hitsContainer.z0[totalIndex];
			const float diffZ = hitZ0 - trackZ;
			const float trackAccurateX = trackX + diffZ * trackTx;
			const float trackAccurateY = trackY + diffZ * trackTy;

			const float hitX0 = hitsContainer.x0[totalIndex];
			const float hitDxdy = hitsContainer.dxdy[totalIndex];
			const float hitAccurateX = hitX0 + hitDxdy * trackAccurateY;

			if (std::abs(hitAccurateX - trackAccurateX) < adaptiveTolerance) {
				gatheredHits.x[writeIndex] = hitAccurateX;
				gatheredHits.z[writeIndex] = hitZ0;
				gatheredHits.index[writeIndex] = totalIndex;
				++writeIndex;
			}

			--count;
		}

		offsets[i] = writeIndex;
	}

	gatheredHits.resize(writeIndex);

	return { std::move(gatheredHits), std::move(offsets) };
}


std::pair<GatheredHits, std::vector<uint32_t>> PrVeloUtOpt::GatherHits(const UtHitSpacePartition& hits, const States& states, const ExtrapolatedStates* layerStates, size_t numLayers, float tolerance) {
	const UtHits& hitsContainer = hits.GetPartitionedHits();
	const size_t numLayerStates = layerStates[0].size();

	size_t writeIndex = 0;

	size_t sumBinSizes = 0;
	std::vector<StateBinLocations> binLocations;
	binLocations.resize(numLayers);
	for (size_t i = 0; i < numLayers; ++i) {
		size_t sumBinSizeSingle;
		binLocations[i] = GetBinLocations(hits, layerStates[i], sumBinSizeSingle);
		sumBinSizes += sumBinSizeSingle;
	}

	GatheredHits gatheredHits;
	std::vector<uint32_t> offsets;
	gatheredHits.resize(sumBinSizes);
	offsets.resize(states.size());

	for (size_t i = 0; i < numLayerStates; ++i) {
		for (size_t layer = 0; layer < numLayers; ++layer) {
			const auto index = binLocations[layer].index[i];
			auto count = binLocations[layer].count[i];
			float trackX = layerStates[layer].x[i];
			float trackY = layerStates[layer].y[i];
			float trackTx = states.tx[i];
			float trackTy = states.ty[i];
			float trackZ = layerStates[layer].z;

			while (count) {
				auto totalIndex = index + count;

				const float hitZ0 = hitsContainer.z0[totalIndex];
				const float diffZ = hitZ0 - trackZ;
				const float trackAccurateX = trackX + diffZ * trackTx;
				const float trackAccurateY = trackY + diffZ * trackTy;

				const float hitX0 = hitsContainer.x0[totalIndex];
				const float hitDxdy = hitsContainer.dxdy[totalIndex];
				const float hitAccurateX = hitX0 + hitDxdy * trackAccurateY;

				if (std::abs(hitAccurateX - trackAccurateX) < tolerance) {
					gatheredHits.x[writeIndex] = hitAccurateX;
					gatheredHits.z[writeIndex] = hitZ0;
					gatheredHits.index[writeIndex] = totalIndex;
					++writeIndex;
				}

				--count;
			}
		}
		offsets[i] = writeIndex;
	}

	gatheredHits.resize(writeIndex);

	return { std::move(gatheredHits), std::move(offsets) };
}


struct SearchLayers {
	uint8_t base1, base2, extra1, extra2;
};


uint32_t FindLinesForTrack(const std::array<GatheredHits, 4>& gatheredHits,
						   const std::array<uint32_t, 4>& beginIndices,
						   const std::array<uint32_t, 4>& endIndices,
						   SearchLayers layerConfig,
						   UtTrackCandidates& outCandidates,
						   size_t writeIndex) {
	size_t writeIndexStart = writeIndex;
	uint32_t minHitsFound = 3;
	uint32_t passedCount = 0;

	for (size_t i0 = beginIndices[layerConfig.base1]; i0 < endIndices[layerConfig.base1]; ++i0) {
		float x0 = gatheredHits[layerConfig.base1].x[i0];
		float z0 = gatheredHits[layerConfig.base1].z[i0];

		for (size_t i2 = beginIndices[layerConfig.base2]; i2 < endIndices[layerConfig.base2]; ++i2) {
			float x2 = gatheredHits[layerConfig.base2].x[i2];
			float z2 = gatheredHits[layerConfig.base2].z[i2];

			float invDiffZ = 1.0f / (z2 - z0);

			float minDiffExtra1 = 0.5f;
			uint32_t minDiffIdx1 = 0;
			for (size_t extra = beginIndices[layerConfig.extra1]; extra < endIndices[layerConfig.extra1]; ++extra) {
				const float xe = gatheredHits[layerConfig.extra1].x[extra];
				const float ze = gatheredHits[layerConfig.extra1].z[extra];
				const float t = (ze - z0) * invDiffZ;
				const float xexp = (1 - t) * x0 + t * x2;
				const float xdiff = std::abs(xexp - xe);
				minDiffIdx1 = xdiff < minDiffExtra1 ? extra : minDiffIdx1;
				minDiffExtra1 = xdiff < minDiffExtra1 ? xdiff : minDiffExtra1;
			}

			float minDiffExtra3 = 0.5f;
			uint32_t minDiffIdx3 = 0;
			for (size_t extra = beginIndices[layerConfig.extra2]; extra < endIndices[layerConfig.extra2]; ++extra) {
				const float xe = gatheredHits[layerConfig.extra2].x[extra];
				const float ze = gatheredHits[layerConfig.extra2].z[extra];
				const float t = (ze - z0) * invDiffZ;
				const float xexp = (1 - t) * x0 + t * x2;
				const float xdiff = std::abs(xexp - xe);
				minDiffIdx3 = xdiff < minDiffExtra3 ? extra : minDiffIdx3;
				minDiffExtra3 = xdiff < minDiffExtra3 ? xdiff : minDiffExtra3;
			}

			int numHitsFound = 2 + (minDiffExtra1 < 0.5f) + (minDiffExtra3 < 0.5f);
			if (numHitsFound == 4 && minHitsFound == 3) {
				minHitsFound = 4;
				writeIndex = writeIndexStart;
				passedCount = 0;
			}
			if (numHitsFound >= minHitsFound) {
				outCandidates.hitAccurateX[layerConfig.base1][writeIndex] = x0;
				outCandidates.hitAccurateX[layerConfig.base2][writeIndex] = x2;
				outCandidates.hitAccurateX[layerConfig.extra1][writeIndex] = gatheredHits[layerConfig.extra1].x[minDiffIdx1];
				outCandidates.hitAccurateX[layerConfig.extra2][writeIndex] = gatheredHits[layerConfig.extra2].x[minDiffIdx3];

				outCandidates.hitZ[layerConfig.base1][writeIndex] = z0;
				outCandidates.hitZ[layerConfig.base2][writeIndex] = z2;
				outCandidates.hitZ[layerConfig.extra1][writeIndex] = gatheredHits[layerConfig.extra1].z[minDiffIdx1];
				outCandidates.hitZ[layerConfig.extra2][writeIndex] = gatheredHits[layerConfig.extra2].z[minDiffIdx3];

				outCandidates.hitWeight[layerConfig.base1][writeIndex] = 1.0f;
				outCandidates.hitWeight[layerConfig.base2][writeIndex] = 1.0f;
				outCandidates.hitWeight[layerConfig.extra1][writeIndex] = minDiffExtra1 < 0.5f;
				outCandidates.hitWeight[layerConfig.extra2][writeIndex] = minDiffExtra3 < 0.5f;

				outCandidates.hitIndex[layerConfig.base1][writeIndex] = gatheredHits[layerConfig.base1].index[i0];
				outCandidates.hitIndex[layerConfig.base2][writeIndex] = gatheredHits[layerConfig.base2].index[i2];
				outCandidates.hitIndex[layerConfig.extra1][writeIndex] = gatheredHits[layerConfig.extra1].index[minDiffIdx1];
				outCandidates.hitIndex[layerConfig.extra2][writeIndex] = gatheredHits[layerConfig.extra2].index[minDiffIdx3];
				++passedCount;
				++writeIndex;
			}
		}
	}

	return passedCount;
}



UtTrackCandidates FindLines(const std::array<GatheredHits, 4>& gatheredHits, const std::array<std::vector<uint32_t>, 4>& gatheredOffests, const States& states) {
	size_t numTracks = gatheredOffests[0].size();
	uint32_t linePassedCount = 0;
	uint32_t trackPassedCount = 0;

	const SearchLayers cfgVariant1 = { 0, 2, 1, 3 };
	const SearchLayers cfgVariant2 = { 1, 3, 0, 2 };

	UtTrackCandidates trackCandidates;
	trackCandidates.resize(numTracks * 4);
	size_t writeIndex = 0;

	std::array<uint32_t, 4> hitIndices = { 0, 0, 0, 0 };
	for (size_t trackIdx = 0; trackIdx < numTracks; ++trackIdx) {
		std::array<uint32_t, 4> currentIndices;
		for (int i = 0; i < 4; ++i) {
			currentIndices[i] = gatheredOffests[i][trackIdx];
		}

		std::array<int, 4> cnts;
		for (auto i = 0; i < 4; ++i) {
			cnts[i] = currentIndices[i] - hitIndices[i];
		}
		int halfProd1 = cnts[0] * cnts[2] * (cnts[1] + cnts[3]);
		int halfProd2 = cnts[1] * cnts[3] * (cnts[0] + cnts[2]);

		const auto& firstCfgVariant = halfProd1 > halfProd2 ? cfgVariant1 : cfgVariant2;
		const auto& secondCfgVariant = halfProd1 > halfProd2 ? cfgVariant2 : cfgVariant1;
		const auto maxProd = std::max(halfProd1, halfProd2);
		const auto minProd = std::min(halfProd1, halfProd2);

		if (trackCandidates.size() < writeIndex + maxProd) {
			trackCandidates.resize(writeIndex + maxProd);
		}

		uint32_t numCandidates = 0;
		if (maxProd > 0) {
			numCandidates = FindLinesForTrack(gatheredHits, hitIndices, currentIndices, firstCfgVariant, trackCandidates, writeIndex);
		}
		if (numCandidates == 0 && minProd > 0) {
			numCandidates = FindLinesForTrack(gatheredHits, hitIndices, currentIndices, secondCfgVariant, trackCandidates, writeIndex);
		}

		for (uint32_t i = 0; i < numCandidates; ++i) {
			trackCandidates.trackX[writeIndex + i] = states.x[trackIdx];
			trackCandidates.trackY[writeIndex + i] = states.y[trackIdx];
			trackCandidates.trackTx[writeIndex + i] = states.tx[trackIdx];
			trackCandidates.trackTy[writeIndex + i] = states.ty[trackIdx];
			trackCandidates.trackZ[writeIndex + i] = states.z[trackIdx];
			trackCandidates.trackIndex[writeIndex + i] = trackIdx;
		}

		writeIndex += numCandidates;
		hitIndices = currentIndices;

		// Debug
		trackPassedCount += numCandidates > 0;
		linePassedCount += numCandidates;
	}

	//std::cout << "passed lines:  " << linePassedCount << std::endl;
	//std::cout << "passed tracks: " << trackPassedCount << std::endl;
	trackCandidates.resize(writeIndex);
	return trackCandidates;
}


constexpr std::array<float, 3> magFieldParams = { 2010.0f, -2240.0f, -71330.f };


void PrintCholesky() {
	/*
	DO  I = 1, N
		A(I,I) = SQRT (A(I, I))
		DO  J = I+1, N
			A(J,I) = A(J,I)/A(I,I)
		END DO
		DO  K=I+1,N
			DO J = K, N
				A(J,K) = A(J,K) - A(J,I)*A(K,I)
			END DO	
		END DO
	END DO
	*/

	auto Linearize = [](int i, int j) -> int {
		constexpr int n = 3;
		if (j < i) {
			std::swap(i, j);
		}
		int idx = j - i;
		for (int q = 0; q < i; ++q) {
			idx += n - q;
		}
		return idx;
	};

	constexpr int n = 3;
	for (int i = 0; i < n; ++i) {
		//mat(i, i) = sqrt(mat(i, i));
		printf("mat(%d, %d) = sqrt(mat(%d, %d))", i, i, i, i);
		printf(" -- mat(%d) = sqrt(mat(%d))", Linearize(i, i), Linearize(i, i));
		std::cout << std::endl;
		for (int j = i + 1; j < n; ++j) {
			//mat(j, i) /= mat(i, i);
			printf("mat(%d, %d) /= mat(%d, %d)", j, i, i, i);
			printf(" -- mat(%d) /= mat(%d)", Linearize(j, i), Linearize(i, i));
			std::cout << std::endl;
		}
		for (int k = i + 1; k < n; ++k) {
			for (int j = k; j < n; ++j) {
				//mat(j, k) -= mat(j, i) * mat(k, i);
				printf("mat(%d, %d) -= mat(%d, %d) * mat(%d, %d)", j, k, j, i, k, i);
				printf(" -- mat(%d) -= mat(%d) * mat(%d)", Linearize(j, k), Linearize(j, i), Linearize(k, i));
				std::cout << std::endl;
			}
		}
	}
}


void CholeskyDecompInplace(std::array<float, 6>& mat) {
	mat[0] = sqrt(mat[0]);
	mat[1] /= mat[0];
	mat[2] /= mat[0];
	mat[3] -= mat[1] * mat[1];
	mat[4] -= mat[2] * mat[1];
	mat[5] -= mat[2] * mat[2];
	mat[3] = sqrt(mat[3]);
	mat[4] /= mat[3];
	mat[5] -= mat[4] * mat[4];
	mat[5] = sqrt(mat[5]);
}

void CholeskySolveInplace(const std::array<float, 6>& mat, std::array<float, 3>& vec) {
	// Ax = b
	// LL^Tx = b

	// Helper variables because I was lazy to regex-replace them in the below code.
	const float& a = mat[0];
	const float& b = mat[1];
	const float& c = mat[2];
	const float& d = mat[3];
	const float& e = mat[4];
	const float& f = mat[5];

	float &b1 = vec[0], &d1 = vec[0];
	float &b2 = vec[1], &d2 = vec[1];
	float &b3 = vec[2], &d3 = vec[2];

	// Solve Ld = b
	b1 /= a;
	b2 -= b1 * b;
	b3 -= b1 * c;

	b2 /= d;
	b3 -= b2 * e;
	b3 /= f;

	// Solve L^Tx = d
	d3 /= f;
	d2 -= d3 * e;
	d1 -= d3 * c;

	d2 /= d;
	d1 -= d2 * b;
	d1 /= a;

	// The solution is now in d=vec.
}


void FitTrackCandidates(const UtTrackCandidates& candidates) {
	size_t numCandidates = candidates.size();

	for (size_t candidateIdx = 0; candidateIdx < numCandidates; ++candidateIdx) {
		const float x = candidates.trackX[candidateIdx];
		const float y = candidates.trackY[candidateIdx];
		const float tx = candidates.trackTx[candidateIdx];
		const float ty = candidates.trackTy[candidateIdx];
		const float z = candidates.trackZ[candidateIdx];

		const float zKink = magFieldParams[0] - ty * ty * magFieldParams[1] - ty * ty * ty * ty * magFieldParams[2];
		const float zDiff = 0.001f * (zKink - zMidUT);
		const float xMidField = x + tx * (zKink - z);
		const float weightVelo = 1.0f; // Set actual value.

		std::array<float, 6> mat = { weightVelo, weightVelo * zDiff, weightVelo * zDiff * zDiff, 0.0f, 0.0f, 0.0f };
		std::array<float, 3> rhs = { weightVelo * xMidField, weightVelo * xMidField * zDiff, 0.0f };

		constexpr float sines[4] = { 0.0f, 0.08715f, -0.08715f, 0.0f };
		for (int layer = 0; layer < 4; ++layer) {
			const float ui = candidates.hitAccurateX[layer][candidateIdx];
			const float dz = 0.001f * (candidates.hitZ[layer][candidateIdx] - zMidUT);
			const float w = candidates.hitWeight[layer][candidateIdx];
			const float t = sines[layer]; // Sine of layer fiber angle.

			mat[0] += w;
			mat[1] += w * dz;
			mat[2] += w * dz * dz;
			mat[3] += w * t;
			mat[4] += w * dz * t;
			mat[5] += w * t * t;
			rhs[0] += w * ui;
			rhs[1] += w * ui * dz;
			rhs[2] += w * ui * t;
		}

		CholeskyDecompInplace(mat);
		CholeskySolveInplace(mat, rhs);

		const float xUTFit = rhs[0];
		const float xSlopeUTFit = 0.001f * rhs[1];
		const float offsetY = rhs[2];
	}
}


inline std::array<float, 2> Solve2x2(const std::array<float, 3>& A, const std::array<float, 2>& b) {
	const float det = A[0] * A[2] - A[1] * A[1];
	const float x1 = (b[0] * A[2] - A[1] * b[1]) / det;
	const float x2 = (A[0] * b[1] - b[0] * A[1]) / det;
	return { x1, x2 };
}


FittedTrackProperties FitTrackCandidatesSimple(const UtTrackCandidates& candidates) {
	size_t numCandidates = candidates.size();
	FittedTrackProperties result;
	result.resize(numCandidates);

	for (size_t candidateIdx = 0; candidateIdx < numCandidates; ++candidateIdx) {
		const float x = candidates.trackX[candidateIdx];
		const float tx = candidates.trackTx[candidateIdx];
		const float ty = candidates.trackTy[candidateIdx];
		const float z = candidates.trackZ[candidateIdx];

		const float zKink = magFieldParams[0] - ty * ty * magFieldParams[1] - ty * ty * ty * ty * magFieldParams[2];
		const float zDiff = 0.001f * (zKink - zMidUT);
		const float xMidField = x + tx * (zKink - z);
		const float weightVelo = 1.0f; // Set actual value.


		std::array<float, 3> mat = { weightVelo, weightVelo * zDiff, weightVelo * zDiff * zDiff };
		std::array<float, 2> rhs = { weightVelo * xMidField, weightVelo * xMidField * zDiff };

		constexpr float cosines[4] = { 1.0f, 0.996194, 0.996194, 1.0f };
		for (int layer = 0; layer < 4; ++layer) {
			const float ui = candidates.hitAccurateX[layer][candidateIdx];
			const float ci = cosines[layer];
			const float dz = 0.001f * (candidates.hitZ[layer][candidateIdx] - zMidUT);
			const float wi = candidates.hitWeight[layer][candidateIdx];

			mat[0] += wi * ci;
			mat[1] += wi * ci * dz;
			mat[2] += wi * ci * dz * dz;

			rhs[0] += wi * ui;
			rhs[1] += wi * ui * dz;
		}

		auto solution = Solve2x2(mat, rhs);

		const float xUTFit = solution[0];
		const float fittedSlopeAtUt = 0.001f * solution[1];
		result.tx[candidateIdx] = fittedSlopeAtUt;
		result.chi2[candidateIdx] = 0.0f;
	}

	return result;
}



} // namespace opt
