#pragma once


#include <vector>
#include <ostream>
#include <algorithm>

#include "Mathter/Vector.hpp"
#include "Mathter/Matrix.hpp"

// Forward declarations

namespace LHCb {
  struct State {
  public:
  	State() = default;
  	State(const mathter::Vector<float,5>& stateVector, float z) : m_stateVector(stateVector), m_z(z) {}

    /// Retrieve the x-position of the state
    float x() const;
  
    /// Retrieve the y-position of the state
    float y() const;
  
    /// Retrieve the z-position of the state
    float z() const;
  
    /// Retrieve the slopes (Tx=dx/dz,Ty=dy/dz,1.) of the state
    mathter::Vector<float, 3> slopes() const;
  
    /// Retrieve the Tx=dx/dz slope of the state
    float tx() const;
  
    /// Retrieve the Ty=dy/dz slope of the state
    float ty() const;
  
    /// Retrieve the charge-over-momentum Q/P of the state
    float qOverP() const { return m_stateVector[4]; }
  
    /// Retrieve the momentum of the state
    float p() const;
  
    /// Retrieve the transverse momentum of the state
    float pt() const;


    /// Update the x-position of the state
    void setX(float value);
  
    /// Update the y-position of the state
    void setY(float value);
  
    /// Update the z-position of the state
    void setZ(float value);
  
    /// Update the x-slope Tx=dx/dz slope of the state
    void setTx(float value);
  
    /// Update the y-slope Ty=dy/dz slope of the state
    void setTy(float value);
  
    /// Update the charge-over-momentum Q/P value of the state
    void setQOverP(float value) { m_stateVector[4] = value; }

  
    /// transport this state to a new z-position
    void linearTransportTo(float z);



    unsigned m_flags; ///< the variety of State flags
    mathter::Vector<float,5> m_stateVector; ///< the state vector
    mathter::Matrix<float,5,5> m_covariance;  ///< the state covariance matrix (indexes 0,...,4 for x, y, tx, ty, Q/p)
    float m_z; ///< the z-position of the state

  
  };

} // namespace LHCb;


inline float LHCb::State::x() const 
{

    return m_stateVector[0];
        
}

inline float LHCb::State::y() const 
{

    return m_stateVector[1];
        
}

inline float LHCb::State::z() const 
{

    return m_z;
        
}

inline mathter::Vector<float,3> LHCb::State::slopes() const
{

    return { m_stateVector[2], m_stateVector[3], 1.f };
        
}

inline float LHCb::State::tx() const 
{

    return m_stateVector[2];
        
}

inline float LHCb::State::ty() const 
{

   return m_stateVector[3];
        
}

inline void LHCb::State::setX(float value) 
{

  m_stateVector[0] = value;
        
}

inline void LHCb::State::setY(float value) 
{

  m_stateVector[1] = value;
        
}

inline void LHCb::State::setZ(float value) 
{

  m_z = value;
        
}

inline void LHCb::State::setTx(float value) 
{

  m_stateVector[2] = value;
        
}

inline void LHCb::State::setTy(float value) 
{

  m_stateVector[3] = value;
        
}
