/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRKERNEL_UTHIT_H
#define PRKERNEL_UTHIT_H
#include "UTHit.h"
#include <vector>


namespace UT {

  namespace Mut
  {
    // Mutable UTHit to allow a modifiable object, needed in some algorithms.
    // Maybe it is also usefull to use a template and merge with the ModPrHit from the seeding
    struct Hit {

      const UT::Hit* HitPtr;
      float x, z;
      float projection;

      Hit( const UT::Hit* ptr, float x, float z ) : HitPtr( ptr ), x( x ), z( z ) {}

      Hit( const UT::Hit* ptr, float x, float z, float proj)
          : HitPtr( ptr ), x( x ), z( z ), projection( proj )
      {}
    };

    typedef std::vector<Hit> Hits;

    inline auto IncreaseByProj = []( const Hit& lhs, const Hit& rhs ) {
      if ( lhs.projection < rhs.projection ) return true;
      if ( rhs.projection < lhs.projection ) return false;
      return lhs.HitPtr->id < rhs.HitPtr->id;
    };
  }
}
#endif
