/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef PRVELOUT_H
#define PRVELOUT_H 1

// from TrackInterfaces

#include "Constants.h"
#include "Hlt1Track.h"
#include "MathUtil.h"
#include "Mathter/Geometry.hpp"
#include "Mathter/Matrix.hpp"
#include "State.h"
#include "UTHit.h"
#include "UTHitHandler.h"
#include "UTMutHit.h"

#include <array>
#include <numeric>
#include <optional>
#include <tuple>



// INTERNAL DATA TO SERIALIZE AND CACHE:
//
// +m_zMidUT = m_PrUTMagnetTool->zMidUT();
// +m_distToMomentum = m_PrUTMagnetTool->averageDist2mom();
//
// These are PrTableFunctions, which is simply a multidimensional array
// const auto& fudgeFactors = m_PrUTMagnetTool->DxLayTable();
// const auto& bdlTable     = m_PrUTMagnetTool->BdlTable();
//
// +LHCb::UTDAQ::computeGeometry(*m_utDet, m_layers, m_sectorsZ);


using SectorsInRegionZ = std::array<float, 98>;
using SectorsInLayerZ = std::array<SectorsInRegionZ, 3>;
using SectorsInStationZ = std::array<SectorsInLayerZ, 2>;


namespace Gaudi::Units {
constexpr float mm = 1.f;
constexpr float GeV = 1e+3f;
constexpr float mrad = 1.e-3;
} // namespace Gaudi::Units

/** @class PrVeloUT PrVeloUT.h
   *
   *  PrVeloUT algorithm. This is just a wrapper,
   *  the actual pattern recognition is done in the 'PrVeloUTTool'.
   *
   *  - InputTracksName: Input location for Velo tracks
   *  - OutputTracksName: Output location for VeloTT tracks
   *  - TimingMeasurement: Do a timing measurement?
   *
   *  @author Mariusz Witek
   *  @date   2007-05-08
   *  @update for A-Team framework 2007-08-20 SHM
   *
   *  2017-03-01: Christoph Hasse (adapt to future framework)
   */


struct MiniState final {
	float x, y, z, tx, ty;
};

struct TrackHelper final {
	TrackHelper(const MiniState& miniState, const float zKink, const float sigmaVeloSlope, const float maxPseudoChi2) : state(miniState),
																														bestParams{ { 0.0, maxPseudoChi2, 0.0, 0.0 } } {
		xMidField = state.x + state.tx * (zKink - state.z);
		const float a = sigmaVeloSlope * (zKink - state.z);
		wb = 1. / (a * a);
		invKinkVeloDist = 1 / (zKink - state.z);
	}

	MiniState state;
	std::array<const UT::Mut::Hit*, 4> bestHits = { nullptr, nullptr, nullptr, nullptr };
	std::array<float, 4> bestParams;
	float wb, invKinkVeloDist, xMidField;
};


struct PrVeloUtOutputWorking {
	std::vector<std::vector<UT::Hit>> hits;
	std::vector<LHCb::State> endVeloStates;
	std::vector<LHCb::State> atUtStates;
	std::vector<LHCb::HLT1::Track> tracks;
};

using PrVeloUtOutput = std::tuple<
	std::vector<std::vector<UT::Hit>>,
	std::vector<LHCb::State>,
	std::vector<LHCb::State>,
	std::vector<LHCb::HLT1::Track>>;

class PrVeloUT {
public:
	void initialize();

	PrVeloUtOutput operator()(const std::vector<LHCb::HLT1::Track>& veloTracks,
							  const std::vector<LHCb::State>& veloEndStates,
							  const UT::HitHandler& hh) const;

private:
	float m_minMomentum{ 1.5 * Gaudi::Units::GeV };
	float m_minPT{ 0.3 * Gaudi::Units::GeV };
	float m_maxPseudoChi2{ 1280. };
	float m_yTol{ 0.5 * Gaudi::Units::mm }; // 0.8
	float m_yTolSlope{ 0.08 }; // 0.2
	float m_hitTol1{ 6.0 * Gaudi::Units::mm };
	float m_hitTol2{ 0.8 * Gaudi::Units::mm }; // 0.8
	float m_deltaTx1{ 0.035 };
	float m_deltaTx2{ 0.018 }; // 0.02
	float m_maxXSlope{ 0.350 };
	float m_maxYSlope{ 0.300 };
	float m_centralHoleSize{ 33. * Gaudi::Units::mm };
	float m_intraLayerDist{ 15.0 * Gaudi::Units::mm };
	float m_overlapTol{ 0.7 * Gaudi::Units::mm };
	float m_passHoleSize{ 40. * Gaudi::Units::mm };
	bool m_printVariables{ false };
	bool m_passTracks{ false };
	bool m_finalFit{ false };

	mutable unsigned m_seedsCounter{ 0 };
	mutable unsigned m_tracksCounter{ 0 };

	void recomputeGeometry();

	bool isTowardsBeampipe(const MiniState& state) const;
	bool isMissingUt(const MiniState& state) const;

	template <typename FudgeTable>
	bool getHits(std::array<UT::Mut::Hits, 4>& hitsInLayers,
				 const UT::HitHandler& hh,
				 const FudgeTable& fudgeFactors, const MiniState& trState) const;

	bool formClusters(const std::array<UT::Mut::Hits, 4>& hitsInLayers, TrackHelper& helper) const;

	template <class BdlTable>
	auto makeSlimTrack(const TrackHelper& helper,
					   const std::array<UT::Mut::Hits, 4>& hitsInLayers,
					   const BdlTable& bdlTable) const
		-> std::optional<std::tuple<std::vector<UT::Hit>, LHCb::State>>;

	// ==============================================================================
	// -- Method that finds the hits in a given layer within a certain range
	// ==============================================================================
	template <typename RANGE>
	void findHits(RANGE range,
				  float zInit,
				  const MiniState& myState, const float xTolNormFact,
				  const float invNormFact, UT::Mut::Hits& hits) const {

		const auto yApprox = myState.y + myState.ty * (zInit - myState.z);
		const auto xOnTrackProto = myState.x + myState.tx * (zInit - myState.z);
		const auto yyProto = myState.y - myState.ty * myState.z;

		auto first = std::find_if_not(range.first, range.second,
									  [y = m_yTol + m_yTolSlope * std::abs(xTolNormFact), yApprox](const auto& h) { return !IsInsideInterval(yApprox, h.yBegin, h.yEnd, y); });
		for (auto last = range.second; first != last; ++first) {
			const auto& hit = *first;

			const auto xx = hit.xAt(yApprox);
			const auto dx = xx - xOnTrackProto;

			if (dx < -xTolNormFact)
				continue;
			if (dx > xTolNormFact)
				break;

			// -- Now refine the tolerance in Y
			float tolerance = m_yTol + m_yTolSlope * std::abs(dx * invNormFact);
			if (!IsInsideInterval(yApprox, hit.yBegin, hit.yEnd, tolerance))
				continue;

			const auto zz = hit.zAtYEq0;
			const auto yy = yyProto + myState.ty * zz;
			const auto xx2 = hit.xAt(yy);
			hits.emplace_back(&hit, xx2, zz);
		}
	}

	// ===========================================================================================
	// -- 2 helper functions for fit
	// -- Pseudo chi2 fit, templated for 3 or 4 hits
	// ===========================================================================================
	void addHit(std::array<float, 3>& mat, std::array<float, 2>& rhs, const UT::Mut::Hit& hit) const {
		const float ui = hit.x;
		const float ci = CosUtFiberAngle(*hit.HitPtr);
		const float dz = 0.001 * (hit.z - m_zMidUT);
		const float wi = hit.HitPtr->weight();
		mat[0] += wi * ci;
		mat[1] += wi * ci * dz;
		mat[2] += wi * ci * dz * dz;
		rhs[0] += wi * ui;
		rhs[1] += wi * ui * dz;
	}



	template <std::size_t N>
	void simpleFit(std::array<const UT::Mut::Hit*, N> hits, TrackHelper& helper) const {
		static_assert(N == 3 || N == 4);


		// -- Scale the z-component, to not run into numerical problems
		// -- with floats
		const float zDiff = 0.001 * (m_zKink - m_zMidUT);
		auto mat = std::array{ helper.wb, helper.wb * zDiff, helper.wb * zDiff * zDiff };
		auto rhs = std::array{ helper.wb * helper.xMidField, helper.wb * helper.xMidField * zDiff };
		std::for_each(hits.begin(), hits.end(), [&](const auto* h) { this->addHit(mat, rhs, *h); });

		// TODO: add LUP solver from Mathter
		//ROOT::Math::CholeskyDecomp<float, 2> decomp(mat.data());
		//if( UNLIKELY(!decomp)) return;


		//decomp.Solve(rhs);
		mathter::Matrix<float, 2, 2> A = {
			mat[0],
			mat[1],
			mat[1],
			mat[2],
		};
		mathter::Vector<float, 2> b = { rhs[0], rhs[1] };
		mathter::Vector<float, 2> x = A.DecompositionLUP().Solve(b);
		rhs[0] = x[0];
		rhs[1] = x[1];


		const float xSlopeTTFit = 0.001 * rhs[1];
		const float xTTFit = rhs[0];

		// new VELO slope x
		const float xb = xTTFit + xSlopeTTFit * (m_zKink - m_zMidUT);
		const float xSlopeVeloFit = (xb - helper.state.x) * helper.invKinkVeloDist;
		const float chi2VeloSlope = (helper.state.tx - xSlopeVeloFit) * m_invSigmaVeloSlope;

		const float chi2TT = std::accumulate(hits.begin(), hits.end(), chi2VeloSlope * chi2VeloSlope,
											 [&](float chi2, const auto* hit) {
												 const float du = (xTTFit + xSlopeTTFit * (hit->z - m_zMidUT)) - hit->x;
												 return chi2 + hit->HitPtr->weight() * (du * du);
											 })
							 / (N + 1 - 2);

		if (chi2TT < helper.bestParams[1]) {
			// calculate q/p
			const float sinInX = xSlopeVeloFit * rsqrt(1. + xSlopeVeloFit * xSlopeVeloFit);
			const float sinOutX = xSlopeTTFit * rsqrt(1. + xSlopeTTFit * xSlopeTTFit);
			const float qp = (sinInX - sinOutX);

			helper.bestParams = { qp, chi2TT, xTTFit, xSlopeTTFit };

			std::copy(hits.begin(), hits.end(), helper.bestHits.begin());
			if (N == 3) {
				helper.bestHits[3] = nullptr;
			}
		}
	}


	///< Counter for timing tool
	int m_veloUTTime{ 0 };

	float m_zMidUT = zMidUT;
	float m_distToMomentum = distToMomentum;
	float m_zKink{ 1780.0 };
	float m_sigmaVeloSlope{ 0.10 * Gaudi::Units::mrad };
	float m_invSigmaVeloSlope{ 10.0 / Gaudi::Units::mrad };

	/// information about the different layers
	std::array<LayerInfo, 4> m_layers = layersInfos;
	std::array<SectorsInStationZ, 2> m_sectorsZ = sectorsZ;
};

#endif // PRVELOUT_H
