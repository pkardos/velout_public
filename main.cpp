#include "PrVeloUT.h"
#include "UtHitSpacePartition.h"
#include "ut_inputs_outputs.h"

#include <chrono>
#include <iostream>
#include "PrVeloUtOpt.h"
#include "DataStructures.h"

#ifdef WIN32
#include <Windows.h>
#endif

#if defined(_DEBUG) || (!defined(__OPTIMIZE__) && defined(__GNUC__))
constexpr int repCount = 1;
#else
constexpr int repCount = 100000;
#endif



int main() {
#ifdef WIN32 
	SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_TIME_CRITICAL);
	SetPriorityClass(GetCurrentProcess(), REALTIME_PRIORITY_CLASS);
#endif

	PrVeloUT veloUtTest;
	opt::PrVeloUtOpt veloUtOptTest;

	std::vector<LHCb::HLT1::Track> veloTracks;
	const std::vector<LHCb::State>& veloStates = g_veloStates;
	UT::HitHandler hitHandler;

	for (auto& hit : g_utHits) {
		hitHandler.AddHit(hit);
	}

	for (auto& segment : g_veloHits) {
		veloTracks.push_back({ &segment, nullptr, nullptr });
	}


	UtHits utHitsSoa = MakeSoaUtHits(g_utHits);
	States veloStatesSoa = MakeSoaStates(g_veloStates);


	auto startTime = std::chrono::high_resolution_clock::now();
	volatile float sum = 0.0f;
	for (int i = 0; i < repCount; ++i) {
		opt::UtHitSpacePartition spacePartition(utHitsSoa);
		auto [hitsOpt, endVeloStatesOpt, atUtStatesOpt, tracksOpt] = veloUtOptTest(veloTracks, veloStatesSoa, spacePartition);


		auto [hits, endVeloStates, atUtStates, tracks] = veloUtTest(veloTracks, veloStates, hitHandler);
	}
	auto endTime = std::chrono::high_resolution_clock::now();



	auto elapsedTime = endTime - startTime;
	std::cout << "Time: " << std::chrono::duration_cast<std::chrono::milliseconds>(elapsedTime).count() << std::endl;

	std::cout << "Avg bin size: " << ((float)veloUtOptTest.binSizeSum / (float)veloUtOptTest.binCountSum) << std::endl;


	return 0;
}