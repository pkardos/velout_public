#pragma once

#define BOOST_PFR_USE_CPP17 1
#include <boost/pfr/precise.hpp>
#include <type_traits>


template <class T>
struct IsStdArray : std::false_type {};

template <class T, size_t N>
struct IsStdArray<std::array<T, N>> : std::true_type {};


template <class Base>
class SoaContainerify : public Base {
public:
	void resize(size_t size);
	void reserve(size_t capacity);
	size_t size() const;

private:
	template <size_t Index>
	void ResizeRecurse(Base& arg, size_t size);
	template <size_t Index>
	void ReserveRecurse(Base& arg, size_t capacity);
};


template <class Base>
void SoaContainerify<Base>::resize(size_t size) {
	ResizeRecurse<0>(static_cast<Base&>(*this), size);
}


template <class Base>
void SoaContainerify<Base>::reserve(size_t capacity) {
	ReserveRecurse<0>(static_cast<Base&>(*this), capacity);
}


template <class Base>
template <size_t Index>
void SoaContainerify<Base>::ResizeRecurse(Base& arg, size_t size) {
	constexpr size_t numFields = boost::pfr::tuple_size_v<std::decay_t<Base>>;
	if constexpr (Index < numFields) {
		if constexpr (!IsStdArray<boost::pfr::tuple_element_t<Index, Base>>::value) {
			boost::pfr::get<Index>(arg).resize(size);
		}
		else {
			for (auto& elem : boost::pfr::get<Index>(arg)) {
				elem.resize(size);
			}
		}
		ResizeRecurse<Index + 1>(arg, size);
	}
}


template <class Base>
template <size_t Index>
void SoaContainerify<Base>::ReserveRecurse(Base& arg, size_t capacity) {
	constexpr size_t numFields = boost::pfr::tuple_size_v<Base>;
	if constexpr (Index < numFields) {
		if constexpr (!IsStdArray<boost::pfr::tuple_element_t<Index, Base>>::value) {
			boost::pfr::get<Index>(arg).reserve(capacity);
		}
		else {
			for (auto& elem : boost::pfr::get<Index>(arg)) {
				elem.reserve(capacity);
			}
		}
		ReserveRecurse<Index + 1>(arg, capacity);
	}
}


template <class Base>
size_t SoaContainerify<Base>::size() const {
	constexpr size_t numFields = boost::pfr::tuple_size_v<Base>;
	if constexpr (numFields > 0) {
		if constexpr (!IsStdArray<boost::pfr::tuple_element_t<0, Base>>::value) {
			return boost::pfr::get<0>((Base&)*this).size();
		}
		else {
			return boost::pfr::get<0>((Base&)*this)[0].size();
		}
	}
	else {
		return 0;
	}
}