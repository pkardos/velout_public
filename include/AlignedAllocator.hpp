#pragma once


#include <cstdint>
#include <cstdlib>
#include <memory>


template <class T>
T SnapUp(T number, T multiple) {
	return ((number + multiple - 1) / multiple) * multiple;
}

template <class T>
T HowMany(T number, T multiple) {
	return ((number + multiple - 1) / multiple);
}


template <class T, int Alignment, bool Init = true>
class AlignedAllocator {
public:
	//    typedefs
	typedef T value_type;

public:
	//    convert an allocator<T> to allocator<U>
	template <typename U>
	struct rebind {
		typedef AlignedAllocator<U, Alignment, Init> other;
	};

public:
	explicit AlignedAllocator() {}

	~AlignedAllocator() {}

	explicit AlignedAllocator(AlignedAllocator const&) {}
	template <typename U>
	explicit AlignedAllocator(AlignedAllocator<U, Alignment, Init> const&) {}

	//    address
	T* address(T& r) { return std::addressof(r); }

	T* address(const T& r) { return std::addressof(r); }

	//    memory allocation
	T* allocate(size_t cnt) {
		//T* ptr = (T*)std::aligned_alloc(Alignment, SnapUp(cnt * sizeof(T), (size_t)Alignment));
		T* ptr = (T*)new std::aligned_storage_t<Alignment, Alignment>[HowMany(sizeof(T) * cnt, (decltype(cnt))Alignment)];
		//T* ptr = reinterpret_cast<pointer>(::operator new(cnt * sizeof(T)));
		return ptr;
	}

	void deallocate(T* p, size_t) {
		//free(p);
		//::operator delete(p);
		delete[] reinterpret_cast<std::aligned_storage_t<Alignment, Alignment>*>(p);
	}

	template <typename U>
	void construct(U* ptr) noexcept(std::is_nothrow_default_constructible<U>::value) {
		if constexpr (Init) {
			::new (static_cast<void*>(ptr)) U;
		}
	}

	template <typename U, typename... Args>
	void construct(U* ptr, Args&&... args) {
		::new (static_cast<void*>(ptr)) U(std::forward<Args>(args)...);
	}

	void destroy(T* ptr) {
		if constexpr (Init) {
			ptr->~T();
		}
	}

	bool operator==(const AlignedAllocator&) { return true; }

	bool operator!=(const AlignedAllocator& a) { return !operator==(a); }
};