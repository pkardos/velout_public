#include "UtHitSpacePartition.h"

#include <cassert>
#include <cstring>


namespace opt {



UtHitSpacePartition::UtHitSpacePartition(const UtHits& hits) {
	auto binning = ComputeBins(hits);
	m_binSizes = binning.binSizes;
	m_binOffsets = ComputeBinOffsets(binning.binSizes);
	m_hits = Partition(hits, binning.hitLayers, binning.hitRows, binning.hitBins, m_binOffsets);
}


BinningResult UtHitSpacePartition::ComputeBins(const UtHits& hits) {
	BinningResult binning;
	memset(&binning.binSizes, 0, sizeof(binning.binSizes));
	binning.hitLayers.resize(hits.size());
	binning.hitRows.resize(hits.size());
	binning.hitBins.resize(hits.size());

	size_t size = hits.size();
	for (size_t i = 0; i < size; ++i) {
		float y0 = hits.y0[i];
		float y1 = hits.y1[i];
		float x0 = hits.x0[i];
		float dxdy = hits.dxdy[i];
		float z0 = hits.z0[i];

		const float yMid = (y0 + y1) / 2.0f;
		const float xAtYMid = x0 + dxdy * yMid;

		const auto layerIdx = impl::LayerFromZ(z0);
		const auto rowIdx = impl::RowFromY(yMid);
		const auto binIdx = impl::BinFromX(xAtYMid);

		binning.binSizes(layerIdx, rowIdx, binIdx)++;
		binning.hitLayers[i] = layerIdx;
		binning.hitRows[i] = rowIdx;
		binning.hitBins[i] = binIdx;
	}

	return binning;
}


BinContainer<uint32_t> UtHitSpacePartition::ComputeBinOffsets(const BinContainer<uint32_t>& binSizes) {
	BinContainer<uint32_t> offsets;
	uint32_t accumulator = 0;

	for (size_t layerIdx = 0; layerIdx < numLayers; ++layerIdx) {
		for (size_t rowIdx = 0; rowIdx < numRows; ++rowIdx) {
			for (size_t binIdx = 0; binIdx < numBins; ++binIdx) {
				uint32_t binSize = binSizes(layerIdx, rowIdx, binIdx);
				offsets(layerIdx, rowIdx, binIdx) = accumulator;
				accumulator += binSize;
			}
		}
	}

	return offsets;
}

UtHits UtHitSpacePartition::Partition(const UtHits& hits,
												  std::vector<uint32_t> hitLayers,
												  std::vector<uint32_t> hitRows,
												  std::vector<uint32_t> hitBins,
												  const BinContainer<uint32_t>& binOffsets) {
	BinContainer<uint32_t> accumulatedOffsets = binOffsets;
	UtHits binnedHits;
	binnedHits.resize(hits.size());

	size_t size = hits.size();
	for (size_t i = 0; i < size; ++i) {
		const auto layerIdx = hitLayers[i];
		const auto rowIdx = hitRows[i];
		const auto binIdx = hitBins[i];
		const auto linearOffset = accumulatedOffsets(layerIdx, rowIdx, binIdx)++; // Note the postfix increase.
		binnedHits.x0[linearOffset] = hits.x0[i];
		binnedHits.y0[linearOffset] = hits.y0[i];
		binnedHits.z0[linearOffset] = hits.z0[i];
		binnedHits.y1[linearOffset] = hits.y1[i];
		binnedHits.dxdy[linearOffset] = hits.dxdy[i];
		binnedHits.error[linearOffset] = hits.error[i];
		binnedHits.id[linearOffset] = hits.id[i];
	}

	return binnedHits;
}

const UtHits& UtHitSpacePartition::GetPartitionedHits() const {
	return m_hits;
}


} // namespace opt
