#include "DataStructures.h"


States MakeSoaStates(const std::vector<LHCb::State>& states) {
	States output;
	auto count = states.size();
	output.resize(count);

	for (size_t i = 0; i < count; ++i) {
		output.x[i] = states[i].x();
		output.y[i] = states[i].y();
		output.tx[i] = states[i].tx();
		output.ty[i] = states[i].ty();
		output.z[i] = states[i].z();
		output.qop[i] = states[i].x();
	}

	return output;
}

UtHits MakeSoaUtHits(const std::vector<UT::Hit>& hits) {
	UtHits output;
	auto count = hits.size();
	output.resize(count);

	for (size_t i = 0; i < count; ++i) {
		output.x0[i] = hits[i].xAtYEq0;
		output.y0[i] = hits[i].yBegin;
		output.z0[i] = hits[i].zAtYEq0;
		output.y1[i] = hits[i].yEnd;
		output.dxdy[i] = hits[i].dxdy;
		output.error[i] = hits[i].error;
		output.id[i] = hits[i].id;
	}

	return output;
}


std::vector<LHCb::State> MakeNormalStates(const States& states) {
	size_t numStates = states.size();
	std::vector<LHCb::State> output;

	for (size_t i = 0; i < numStates; ++i) {
		LHCb::State state;
		state.setX(states.x[i]);
		state.setY(states.y[i]);
		state.setTx(states.tx[i]);
		state.setTy(states.ty[i]);
		state.setZ(states.z[i]);
		state.setQOverP(states.qop[i]);
		output.push_back(state);
	}

	return output;
}


std::vector<UT::Hit> MakeNormalUtHits(const UtHits& hits) {
	size_t numStates = hits.size();
	std::vector<UT::Hit> output;

	for (size_t i = 0; i < numStates; ++i) {
		UT::Hit hit;
		hit.xAtYEq0 = hits.x0[i];
		hit.yBegin = hits.y0[i];
		hit.zAtYEq0 = hits.z0[i];
		hit.yEnd = hits.y1[i];
		hit.dxdy = hits.dxdy[i];
		hit.error = hits.error[i];
		hit.id = LHCb::UTChannelID(hits.id[i]);
		output.push_back(hit);
	}

	return output;
}
