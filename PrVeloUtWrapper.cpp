#include "PrVeloUtWrapper.h"
#include "UtHitSpacePartition.h"

namespace wrapper {


inline PrVeloUtOutput PrVeloUtWrapper::operator()(const std::vector<LHCb::HLT1::Track>& veloTracks, const std::vector<LHCb::State>& veloEndStates, const UT::HitHandler& hh) const {
	std::vector<UT::Hit> hits;

	for (int station = 1; station <= 2; ++station) {
		for (int layer = 1; layer <= 2; ++layer) {
			for (int region = 1; region <= 2; ++region) {
				for (int sector = 1; sector <= 2; ++sector) {
					auto range = hh.hits(station, layer, region, sector);
					for (auto it = range.first; it != range.second; ++it) {
						hits.push_back(*it);
					}
				}
			}
		}
	}

	auto soaHits = MakeSoaUtHits(hits);
	auto soaStates = MakeSoaStates(veloEndStates);

	opt::UtHitSpacePartition partitionedHits(soaHits);
	auto [trackHits, trackHitOffets, trackVeloStates, trackVeloTracks] = m_optimized(veloTracks, soaStates, partitionedHits);



}


} // namespace wrapper