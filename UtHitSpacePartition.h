#pragma once

#include "DataStructures.h"

#include <array>
#include <vector>


namespace opt {


constexpr size_t numRows = 14;
constexpr size_t numBins = 128;
constexpr size_t numLayers = 4;
constexpr float detectorBottom = -1386.f / 2.0f;
constexpr float detectorTop = +1386.f / 2.0f;
constexpr float detectorLeft = -1800.f / 2.0f;
constexpr float detectorRight = +1800.f / 2.0f;
constexpr float detectorLayers[numLayers] = { 2327.5f, 2372.5f, 2597.5f, 2642.5f };


template <class T>
struct BinContainer {
	T& operator()(uint32_t layer, uint32_t row, uint32_t bin) {
		return bins[layer][row][bin];
	}
	T operator()(uint32_t layer, uint32_t row, uint32_t bin) const {
		return bins[layer][row][bin];
	}
	std::array<std::array<std::array<T, numBins>, numRows>, numLayers> bins;
};


struct BinningResult {
	BinContainer<uint32_t> binSizes;
	std::vector<uint32_t> hitLayers;
	std::vector<uint32_t> hitRows;
	std::vector<uint32_t> hitBins;
};


class UtHitSpacePartition {
public:
	UtHitSpacePartition(const UtHits& hits);

	inline std::pair<uint32_t, uint32_t> GetBin(float x, float y, float z) const;
	inline std::pair<uint32_t, uint32_t> GetDoubleBin(float x, float y, float z) const;
	const UtHits& GetPartitionedHits() const;

private:
	static BinningResult ComputeBins(const UtHits& hits);
	static BinContainer<uint32_t> ComputeBinOffsets(const BinContainer<uint32_t>& binSizes);
	static UtHits Partition(const UtHits& hits,
							std::vector<uint32_t> hitLayers,
							std::vector<uint32_t> hitRows,
							std::vector<uint32_t> hitBins,
							const BinContainer<uint32_t>& binOffsets);

private:
	UtHits m_hits;
	BinContainer<uint32_t> m_binOffsets;
	BinContainer<uint32_t> m_binSizes;
};



namespace impl {

	inline uint32_t LayerFromZ(float z) {
		constexpr float gate1 = (detectorLayers[0] + detectorLayers[1]) / 2.0f;
		constexpr float gate3 = (detectorLayers[2] + detectorLayers[3]) / 2.0f;
		constexpr float gateDistance = (gate3 - gate1) / 2.0f;
		constexpr float gate0 = gate1 - gateDistance;
		constexpr float gate4 = gate3 + gateDistance;
		constexpr float span = 1.0f / (gate4 - gate0);

		float t = (z - gate0) * span;
		assert(0 <= t && t < 1);
		float layerIdxReal = t * numLayers;
		uint32_t layerIdx = uint32_t(layerIdxReal);
		return layerIdx;
	}


	inline uint32_t RowFromY(float y) {
		constexpr float span = 1.0f / (detectorTop - detectorBottom);
		float t = (y - detectorBottom) * span;
		assert(0 <= t && t < 1);
		float rowIdxReal = t * numRows;
		uint32_t rowReal = uint32_t(rowIdxReal);
		return rowReal;
	}


	inline uint32_t BinFromX(float x) {
		constexpr float span = 1.0f / (detectorRight - detectorLeft);
		float t = (x - detectorLeft) * span;
		assert(0 <= t && t < 1);
		float binIdxReal = t * numBins;
		uint32_t binReal = uint32_t(binIdxReal);
		return binReal;
	}
} // namespace impl






inline std::pair<uint32_t, uint32_t> UtHitSpacePartition::GetBin(float x, float y, float z) const {
	const auto layerIdx = impl::LayerFromZ(z);
	const auto rowIdx = impl::RowFromY(y);
	const auto binIdx = impl::BinFromX(x);

	uint32_t offest = m_binOffsets(layerIdx, rowIdx, binIdx);
	uint32_t size = m_binSizes(layerIdx, rowIdx, binIdx);

	return { offest, size };
}


inline std::pair<uint32_t, uint32_t> UtHitSpacePartition::GetDoubleBin(float x, float y, float z) const {
	constexpr float binWidth = (detectorRight - detectorLeft) / numBins;

	float minLimit = detectorLeft;
	float maxLimit = detectorRight - binWidth - 0.001f;
	float target = x - binWidth / 2.f;
	float xHalfOff = std::min(target, maxLimit);
	xHalfOff = std::max(target, minLimit);

	const auto layerIdx = impl::LayerFromZ(z);
	const auto rowIdx = impl::RowFromY(y);
	const auto binIdx = impl::BinFromX(xHalfOff);

	uint32_t offest = m_binOffsets(layerIdx, rowIdx, binIdx);
	uint32_t size = m_binSizes(layerIdx, rowIdx, binIdx) + m_binSizes(layerIdx, rowIdx, binIdx + 1);

	return { offest, size };
}




} // namespace opt