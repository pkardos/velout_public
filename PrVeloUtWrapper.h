#pragma once

#include "Hlt1Track.h"
#include "PrVeloUtOpt.h"
#include "State.h"
#include "UTHit.h"
#include "UTHitHandler.h"

#include <tuple>


namespace wrapper {


struct PrVeloUtOutputWorking {
	std::vector<std::vector<UT::Hit>> hits;
	std::vector<LHCb::State> endVeloStates;
	std::vector<LHCb::State> atUtStates;
	std::vector<LHCb::HLT1::Track> tracks;
};


using PrVeloUtOutput = std::tuple<
	std::vector<std::vector<UT::Hit>>,
	std::vector<LHCb::State>,
	std::vector<LHCb::State>,
	std::vector<LHCb::HLT1::Track>>;


class PrVeloUtWrapper {
public:
	PrVeloUtOutput operator()(const std::vector<LHCb::HLT1::Track>& veloTracks,
							  const std::vector<LHCb::State>& veloEndStates,
							  const UT::HitHandler& hh) const;

private:
	opt::PrVeloUtOpt m_optimized;
};



} // namespace wrapper